<?php
include_once '../conf/conf.php';

class grados_model{
  public $name_table = '';
  public $attribute_insert_update = array();
  public $conditionality = '';
  public $query_search = '';
  public $id_delete = 0;

  //Metodo Construcctor para instaciar la clase operation_db_model
  function __construct($name_table_ob='',
                       $attribute_insert_update_ob='',
                       $conditionality_ob='',
                       $query_search_ob='',
                       $id_delete_ob = 0){

		$this->name_table = $name_table_ob;
		$this->attribute_insert_update = $attribute_insert_update_ob;
		$this->conditionality = $conditionality_ob;
		$this->query_search = $query_search_ob;
		$this->id_delete = $id_delete_ob;

	}
  function insert_object(){
		$datos_array = $this->attribute_insert_update;
		//$imprimir .= '<pre>'.print_r($datos_array).'</pre>';
		$objeto_conexion = new clase_configuracion();
		$sql_insert = $objeto_conexion->insert($this->name_table,$datos_array);
		$resultado = $objeto_conexion->ejecutar($sql_insert);
    //echo $resultado;die();
		if($resultado){
      $sql = 'SELECT id FROM '.$this->name_table.' ORDER BY id DESC LIMIT 1;';
      $id_res = $objeto_conexion->ejecutar($sql);
      return $id_res[0][0];
		}else{
			return false;
		}
	}

  function update_object(){
		$datos_array = $this->attribute_insert_update;
		$objeto_conexion = new clase_configuracion();
		$sql_update = $objeto_conexion->update($this->name_table,
                                           $datos_array,
                                           $this->conditionality);
		$resultado = $objeto_conexion->ejecutar($sql_update);
		if($resultado){
			return true;
		}else{
			return false;
		}
	}

  function delete_object(){
		$objeto_conexion = new clase_configuracion();
		$sql_delete = $objeto_conexion->delete($this->name_table,$this->id_delete);
    //echo $sql_delete;die();
		$resultado = $objeto_conexion->ejecutar($sql_delete);
    //echo $resultado;
		if($resultado){
			return true;
		}else{
			return false;
		}
	}

  function search(){
		$objeto_conexion = new clase_configuracion();
		$sql = $this->query_search;
		$resultado = $objeto_conexion->ejecutar($sql);
		//echo $resultado;
		if($resultado){
			return $resultado;
		}else{
			return false;
		}
	}

  function search_all(){
    $objeto_conexion = new clase_configuracion();
    $sql = 'select * from grados order by id';
    $resultado = $objeto_conexion->ejecutar($sql);
    if($resultado){
			return $resultado;
		}else{
			return false;
		}
  }

}
//$array_insert = array('nombre_grado' => 'Noveno grado');
//$prueba_object = new grados_model('grados',$array_insert);
//$prueba_insert = $prueba_object->insert_object();
//if($prueba_insert){
//  echo $prueba_insert;
//}else{
//  echo 'error no se inserto';
//}

?>
