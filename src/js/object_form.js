$(document).ready(function(){
  //esta parte es para consultar los datos que estan en la bd y mostralo en la vista
  //*****************search inicia aqui***************************//
  var arreglo_ini = {
    'ruta':0
  }
  $.post( "../controller/grados_controller.php", arreglo_ini, function( data ) {
    console.log(data);
    $('#table_name_grados').append(data.res);
  }, "json");

  //*****************Search termina aqui**************************//

  //*******************Insert/editar incia aqui*************************//
  $('#guardar').on('click',function(){
    if($('#grado').val() == '' || $('#grado').val() == undefined || $('#grado').val() == null){
      alert('El nombre del Grado no puede estar vácio');
    }
    if($('#id_grado').val() == ''){
      var arreglo_add_edit = {
        'ruta':1,
        'nombre_grado':$('#grado').val()
      }
    }else{
      var arreglo_add_edit = {
        'ruta':2,
        'nombre_grado':$('#grado').val(),
        'id_grado':$('#id_grado').val()
      }
    }

    $.post( "../controller/grados_controller.php", arreglo_add_edit, function( data ) {

        if(data.cod == 1){
             if($('#id_grado').val() == ''){
               var row_table = '<tr id="tr_'+data.id+'"><td>'+data.id+'</td>';
               row_table += '<td>'+$('#grado').val()+'</td>';
               row_table += '<td><input type="hidden" id="editar_id_'+data.id+'" value="'+data.id+'" />';
               row_table += '<input type="hidden" id="editar_grado_'+data.id+'" value="'+$('#grado').val()+'" />';
               row_table += '<input type="button" name="Editar" object_form_id="'+data.id+'" class="edit_pos" value="Editar" /></td></tr>';
               $('#table_name_grados').append(row_table);
               $('#grado').val('');
             }else{
               var row_table = '<tr id="tr_'+data.id+'"><td>'+data.id+'</td>';
               row_table += '<td>'+$('#grado').val()+'</td>';
               row_table += '<td><input type="hidden" id="editar_id_'+data.id+'" value="'+data.id+'" />';
               row_table += '<input type="hidden" id="editar_grado_'+data.id+'" value="'+$('#grado').val()+'" />';
               row_table += '<input type="button" name="Editar" object_form_id="'+data.id+'" class="edit_pos" value="Editar" /></td></tr>';
               $('#tr_'+data.id).replaceWith(row_table);
               $('#grado').val('');
               $('#id_grado').val('');
             }

        }else{
          alert('Error: Dato no se insertó');
        }

    },"json");

  });
  //**********************Insert/edit Finaliza Aqui *******************//


});

//*****método para seleccionar el grado que se quiere editar Comienza Aqui ***//
$(document).on('click', '.edit_pos', function(event){
  var boton=$(this); //asi se seleciona el boton que esta haciendo clcik
  var object_form_id=boton.attr('object_form_id'); // aqui se llama al atributo partner_id del boton que se selecciono
  var name_grado = $('#editar_grado_'+object_form_id).val();
  var id_grado = $('#editar_id_'+object_form_id).val();

  $('#id_grado').val(id_grado);
  $('#grado').val(name_grado);
});
