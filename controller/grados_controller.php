<?php
include_once '../model/grados_model.php';

$ruta = $_POST['ruta'];
$nombre_grado_vista = $_POST['nombre_grado'];
$id_grado = $_POST['id_grado'];

if($ruta == 0){
  $object = new grados_model('','','','',0);
  $resultado = $object->search_all();
  $datos_form = '';
  foreach ($resultado as $datos) {
    $datos_form .= '<tr id="tr_'.$datos['id'].'"><td>'.$datos['id'].'</td>';
    $datos_form .= '<td>'.$datos['nombre_grado'].'</td>';
    $datos_form .= '<td><input type="hidden" id="editar_id_'.$datos['id'].'" value="'.$datos['id'].'" />';
    $datos_form .= '<input type="hidden" id="editar_grado_'.$datos['id'].'" value="'.$datos['nombre_grado'].'" />';
    $datos_form .= '<input type="button" name="Editar" class="edit_pos" object_form_id="'.$datos['id'].'" value="Editar" /></td></tr>';
  }
  $response = array(
	   'res' => $datos_form
		);
	  echo json_encode($response);//retorna el areglo $response
	  die();
  //print_r($resultado[1]['nombre_grado']);
}

if($ruta == 1){
  $array_insert = array('nombre_grado' => $nombre_grado_vista );
  $object = new grados_model('grados',$array_insert);
  $val_insert = $object->insert_object();
  if($val_insert){
    $cod = 1;
    $id = $val_insert;
    $msn = '';
  }else{
    $cod = 0;
    $id = $val_insert;
    $msn = 'Error: Dato no se inserto';
  }
  $response = array(
     'cod' => $cod,
	   'id' => $id,
     'msn'=> $msn
	);
	echo json_encode($response);//retorna el areglo $response
	die();

}

if($ruta == 2){
  $array_update = array('nombre_grado' => $nombre_grado_vista );
  $object = new grados_model('grados',$array_update,'id = '.$id_grado);
  $val_update = $object->update_object();
  if($val_update){
    $cod = 1;
    $id = $id_grado;
    $msn = '';
  }else{
    $cod = 0;
    $id = $id_grado;
    $msn = 'Error: Dato no se actualizo';
  }
  $response = array(
     'cod' => $cod,
	   'id' => $id,
     'msn'=> $msn
	);
	echo json_encode($response);//retorna el areglo $response
	die();
}


 ?>
