<?php

class conexion_pdo{
	public $conn = '';
	function __construct($str_conxion,$user,$clave){
		try{
			$this->conn = new PDO($str_conxion, $user, $clave);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $e){
				echo "ERROR: " . $e->getMessage();
			}
	}
	
	function execute_querys($conn,$sql){
		try{
			$sql_res = $this->conn->prepare($sql);
			$sql_res->execute();
			$resu = strpos($sql, 'INSERT');
			if($resu !== FALSE){
				return true;
			}
			$resultado  = $sql_res->fetchAll();
			return $resultado;
			
		}catch(PDOException $e){
				echo "ERROR: " . $e->getMessage();
		}
	}
}
