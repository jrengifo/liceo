<?php
include_once '../conf/conexion_pdo.php';

class clase_configuracion{

	public $host = 'localhost';
	public $port = '5432';
	public $nombre_bd = 'tecno';
	public $usua = 'postgres';
	public $clave = '123456';
	public $connec ='';

	public function __construct(){
		//'pgsql:dbname=test host=localhost'
		$str_conexion = "pgsql:dbname=$this->nombre_bd;host=$this->host";
		$this->connec = new conexion_pdo(
										$str_conexion,
										$this->usua,
										$this->clave );
	}

	public function ejecutar($sql){
		//echo $sql;
		$resul = $this->connec->execute_querys($this->connec,$sql);
		return $resul;
	}

	//Metodo que se encarga de Generar lo query para insertar
	public function insert($nomTabla,$row) {
	  $sql = "INSERT INTO $nomTabla";

	  $sql.= " (".implode(",", array_keys($row)).")";

	  $sql.= " VALUES (";
	  $numero = count($row);
	  foreach ($row as $key => $value) {
			 if(is_numeric($value)){
				 $sql.=" ".$value;
			 } else {
				 $sql.= "'$value'";
			 }
			 $aux +=1;
			 if($numero == $aux){
				 $sql.=") ";
			 }else {
				 $sql.=",";
			 }
	  }

	  return $sql.';';

	}


	//Metodo que se encarga de Generar lo query para actualizar
	function update($nombreTabla, $row, $where) {
		$numero = count($row);
		$sql="UPDATE ".$nombreTabla ." SET ";
		foreach ($row as $key => $value){
			if(is_numeric($value)){
				$sql.= $key." = ".$value;
			}
			else
			{
				$sql.= $key." = '".$value."'";
			}
			 $aux +=1;
			 if($numero == $aux){
				 $sql.=" WHERE ";
			 }else {
				 $sql.=",";
			 }
		}
		$sql.= $where;

		return $sql;
	}

	//Metodo que se encarga de Generar lo query para eliminar registro
	function delete($nombreTabla, $id) {
		$sql = 'DELETE FROM '.$nombreTabla.' WHERE id = '. $id;
		return $sql;
	}

}

//$var = new clase_configuracion();
